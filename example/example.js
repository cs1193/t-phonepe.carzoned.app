import IndexModule from '../source/index';

import {ExampleDirective} from './example.directive';
import {ExampleDirectiveController} from './example.directive.controller';

export default angular.module('example', [
    IndexModule
  ])
  .directive(ExampleDirective.NAME, ExampleDirective.factory)
  .controller(ExampleDirectiveController.NAME, ExampleDirectiveController)
  .name;
