import ExampleDirectiveTemplate from './example.directive.html';
import {ExampleDirectiveController} from './example.directive.controller';

export class ExampleDirective {
  static NAME: string = "example";

  static factory () {
    return {
      restrict: 'E',
      replace: true,
      scope: {},
      transclude: true,
      template: ExampleDirectiveTemplate,
      controller: ExampleDirectiveController,
      controllerAs: 'vm',
      bindToController: true
    };
  }
}
