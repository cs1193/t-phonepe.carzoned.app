const LOGIN_URL = "/fake-login";
const LOGOUT_URL = "/fake-logout";
const REGISTER_URL = "/fake-register";
const RIDE_ENDPOINT = '/rides';
const CONFIRM_RIDE_ENDPOINT = '/confirm-ride';
