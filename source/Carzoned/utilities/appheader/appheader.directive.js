import AppHeaderTemplate from './appheader.directive.html';

export class AppHeaderDirective {
  static NAME: string = 'appHeader';

  static factory () {
    return {
      restrict: 'E',
      replace: true,
      template: AppHeaderTemplate
    };
  }
}
