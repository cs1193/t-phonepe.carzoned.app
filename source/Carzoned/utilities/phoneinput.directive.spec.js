import UtilitiesModule from './utilities.module';

describe('Directtive: Phone Input', () => {
  let $rootScope;
  let $compile;
  let scope;
  let element;
  let form;

  beforeEach(angular.mock.module(UtilitiesModule));

  beforeEach(inject((
    _$rootScope_,
    _$compile_
  ) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;
    scope = $rootScope.$new();
  }));

  function compileElement () {
    element = angular.element('<form name="form"><input type="text" ng-model="phone" name="phone" email-address required /></form>');
    element = $compile(element)(scope);
    form = scope.form;
    $rootScope.$digest();
  }

  // it('should phone', () => {
  //   compileElement();
  //   form.phone.$setViewValue('9999999999');
  //   expect(scope.phone).toEqual('99999 99999');
  // });
});
