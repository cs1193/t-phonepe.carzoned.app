export class TimeAwayFilter {
  static NAME: string = 'timeaway';

  static factory () {
    return (input) => {
      return (input) ? new Date(input).getMinutes() + ' mins away' : 'Time Incorrect';
    }
  }
}
