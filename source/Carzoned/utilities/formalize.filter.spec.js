import UtilitiesModule from './utilities.module';
import {FormalizeFilter} from './formalize.filter';

describe('Filter: Formalize Text', () => {
  let $filter;

  beforeEach(angular.mock.module(UtilitiesModule));

  beforeEach(inject((
    _$filter_
  ) => {
    $filter = _$filter_;
  }));

  it('should be registered', () => {
    expect($filter('formalize')).toBeDefined();
  });

  it('should capitalize the first letter of the word', () => {
    expect($filter('formalize')('minivan')).toBe('Minivan');
  });

  it('should capitalize the first letter of the first word in a sentence', () => {
    expect($filter('formalize')('it is an example')).toBe('It is an example');
  });
})
