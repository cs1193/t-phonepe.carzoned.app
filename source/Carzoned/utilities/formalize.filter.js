export class FormalizeFilter {
  static NAME: string = 'formalize';

  static factory () {
    return (input) => {
      return input.charAt(0).toUpperCase() + input.substr(1).toLowerCase();
    }
  }
}
