import {AppHeaderDirective} from './appheader/appheader.directive';
import {AppFooterDirective} from './appfooter/appfooter.directive';

import {PhoneInputDirective} from './phoneinput.directive';
import {EmailAddressDirective} from './emailaddress.directive';

import {UserProfileDirective} from './userprofile/userprofile.directive';
import {LoadingSpinnerDirective} from './loadingspinner/loadingspinner.directive';

import {FormalizeFilter} from './formalize.filter';
import {TimeAwayFilter} from './timeaway.filter';

export default angular.module('carzoned.utilities', [])
  .directive(AppHeaderDirective.NAME, AppHeaderDirective.factory)
  .directive(AppFooterDirective.NAME, AppFooterDirective.factory)
  .directive(PhoneInputDirective.NAME, PhoneInputDirective.factory)
  .directive(EmailAddressDirective.NAME, EmailAddressDirective.factory)
  .directive(UserProfileDirective.NAME, UserProfileDirective.factory)
  .directive(LoadingSpinnerDirective.NAME, LoadingSpinnerDirective.factory)
  .filter(FormalizeFilter.NAME, FormalizeFilter.factory)
  .filter(TimeAwayFilter.NAME, TimeAwayFilter.factory)
  .name
