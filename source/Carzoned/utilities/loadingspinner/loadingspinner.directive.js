import LoadingSpinnerTemplate from './loadingspinner.directive.html';

export class LoadingSpinnerDirective {
  static NAME: string = 'loadingSpinner';

  static factory () {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        width: '=',
        height: '='
      },
      template: LoadingSpinnerTemplate
    };
  }
}
