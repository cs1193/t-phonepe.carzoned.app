export class PhoneInputDirective {
  static NAME: string = 'phoneInput';

  static factory () {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, element, attributes, controller) {
        controller.$parsers.push((number) => {
          let transformedNumber = number;

          // xxxxx xxxxx
          if(number.match(/^[\d\s]{10}$/)) {
            transformedNumber = number.slice(0, 5) + " " + number.slice(4);
          }

          if (number.length > 11) {
            transformedNumber = number.slice(0, 11);
          }

          if (transformedNumber !== number) {
            controller.$setViewValue(transformedNumber);
            controller.$render();
          }

          return true;
        });
      }
    };
  }
}
