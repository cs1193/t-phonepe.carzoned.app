import UserProfileTemplate from './userprofile.directive.html';

export class UserProfileDirective {
  static NAME: string = 'userProfile';

  static factory () {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        name: '@',
        lastLoggedIn: '@' // Add time filter
      },
      template: UserProfileTemplate
    };
  }
}
