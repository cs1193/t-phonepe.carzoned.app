import AppFooterTemplate from './appfooter.directive.html';

export class AppFooterDirective {
  static NAME: string = 'appFooter';

  static factory () {
    return {
      restrict: 'E',
      replace: true,
      template: AppFooterTemplate,
      link: function (scope, element, attributes, controller) {
        scope.copyrightDate = new Date();
      }
    };
  }
}
