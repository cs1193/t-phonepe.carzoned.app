import UtilitiesModule from './utilities.module';
import {TimeAwayFilter} from './timeaway.filter';

describe('Filter: Time Away', () => {
  let $filter;

  beforeEach(angular.mock.module(UtilitiesModule));

  beforeEach(inject((
    _$filter_
  ) => {
    $filter = _$filter_;
  }));

  it('should be registered', () => {
    expect($filter('timeaway')).toBeDefined();
  });

  it('should return incorrect error if not date instance', () => {
    expect($filter('timeaway')('')).toBe('Time Incorrect');
  });

  it('should return correct data if date instance', () => {
    var futureDate = new Date();
    futureDate.setMinutes(futureDate.getMinutes() + 30)
    expect($filter('timeaway')(futureDate)).toBe(futureDate.getMinutes() + ' mins away');
  });
});
