import LocationSearchTemplate from './locationsearch.directive.html';
import {LocationSearchDirectiveController} from './locationsearch.directive.controller';

export class LocationSearchDirective {
  static NAME: string = 'locationSearch';

  static factory () {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        search: '=',
        placeholder: '@',
        coords: '='
      },
      template: LocationSearchTemplate
    };
  }
}
