export class GooglePlaceDirective {
  static NAME: string = 'googlePlace';

  static factory () {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, element, attributes, controller) {
        let options = {
          types: [],
          componentRestrictions: {
            'country': 'in'
          }
        };

        scope.googlePlace = new google.maps.places.Autocomplete(element[0], options);

        google.maps.event.addListener(scope.googlePlace, 'place_changed', () => {
          scope.coords = [scope.googlePlace.getPlace().geometry.location.lat(), scope.googlePlace.getPlace().geometry.location.lng()];
          scope.$apply(() => {
            controller.$setViewValue(element.val());
          });
        });
      }
    };
  }
}
