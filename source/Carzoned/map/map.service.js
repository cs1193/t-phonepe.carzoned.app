export class MapService {
  static NAME: string = 'MapService';

  /* @ngInject */
  constructor () {
    this.map = null;
    this.markers = [];
    this.bounds = new google.maps.LatLngBounds();
    this.directionService = new google.maps.DirectionsService();
  }

  setMap (map) {
    this.map = map;
  }

  getMap () {
    if (this.map) return this.map;
    throw new Error('map not defined');
  }

  addMarker (index, data) {
    let marker = new google.maps.Marker({
      position: new google.maps.LatLng(data[0], data[1]),
      map: this.map
    });

    if (index < 2) {
      this.markers[index] = marker;
      this.bounds.extend(marker.getPosition());
    }
  }

  deleteMarker (index) {
    if (this.markers[index]) {
      this.markers[index].setMap(null);
    }
  }

  updateMarker (index, data) {
    if (this.markers[index]) {
      this.deleteMarker(index);
    }
    this.addMarker(index, data);
    this.updateRoute();
  }

  updateRoute () {
    if (this.markers.length === 2) {

      this.directionService.route({
        origin: new google.maps.LatLng(this.markers[0].getPosition().lat(), this.markers[0].getPosition().lng()),
        destination: new google.maps.LatLng(this.markers[1].getPosition().lat(), this.markers[1].getPosition().lng()),
        travelMode: google.maps.DirectionsTravelMode.DRIVING
      }, (result, status) => {
        if (status == google.maps.DirectionsStatus.OK) {
          let path = new google.maps.MVCArray();
          let poly = new google.maps.Polyline({
            map: this.map,
            geodesic: true,
            strokeColor: '#ffffff'
          });

          for (let i = 0, routeLength = result.routes[0].overview_path.length; i < routeLength; i++) {
            path.push(result.routes[0].overview_path[i]);
          }

          poly.setPath(path);
        }
      });
      this.map.fitBounds(this.bounds);
    }
  }
}
