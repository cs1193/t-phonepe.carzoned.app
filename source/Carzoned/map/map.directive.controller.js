import {GeoLocationService} from './geolocation.service';
import {MapService} from './map.service';

export class MapDirectiveController {
  static NAME: string = 'MapDirectiveController';

  /* @ngInject */
  constructor ($log, $scope, GeoLocationService, MapService) {
    let vm = this;

    this.$log = $log;
    this.$scope = $scope;
    this.GeoLocationService = GeoLocationService;
    this.MapService = MapService;

    this.markers = [];

    GeoLocationService.getCurrentPosition().then((position) => {
      vm.latitude = position.coords.latitude;
      vm.longitude = position.coords.longitude;
    });

    $scope.$watch(() => {
      return vm.locationSearch.originCoords
    }, (newValue, oldValue) => {
      if (oldValue !== newValue) {
        this.MapService.updateMarker(0, newValue);
      }
    });

    $scope.$watch(() => {
      return vm.locationSearch.destinationCoords
    }, (newValue, oldValue) => {
      if (oldValue !== newValue) {
        this.MapService.updateMarker(1, newValue);
      }
    });
  }

  registerMap (map) {
    this.MapService.setMap(map);
  }
}
