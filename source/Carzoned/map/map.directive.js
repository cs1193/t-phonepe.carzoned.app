import MapDirectiveTemplate from './map.directive.html';
import {MapDirectiveController} from './map.directive.controller';

export class MapDirective {
  static NAME: string = 'map';

  static factory () {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        latitude: '@',
        longitude: '@',
        locationSearch: '='
      },
      controller: MapDirectiveController.NAME,
      controllerAs: 'vm',
      bindToController: true,
      template: MapDirectiveTemplate,
      link: function (scope, element, attributes, controller) {
        let map;

        let mapStyle = [{
          "featureType":"all",
          "elementType":"all",
          "stylers":[{
            "visibility":"on"
          }]
        }, {
          "featureType":"all",
          "elementType":"labels",
          "stylers": [{
            "visibility":"off"
          }, {
            "saturation":"-100"
          }]
        }, {
          "featureType":"all",
          "elementType":"labels.text.fill",
          "stylers": [{
            "saturation":36
          }, {
            "color":"#000000"
          }, {
            "lightness":40
          }, {
            "visibility":"off"
          }]
        }, {
          "featureType":"all",
          "elementType":"labels.text.stroke",
          "stylers":[{
            "visibility":"off"
          }, {
            "color":"#000000"
          }, {
            "lightness":16
          }]
        }, {
          "featureType":"all",
          "elementType":"labels.icon",
          "stylers":[{
            "visibility":"off"
          }]
        }, {
              "featureType":"administrative",
              "elementType":"geometry.fill",
              "stylers":[
                 {
                    "color":"#000000"
                 },
                 {
                    "lightness":20
                 }
              ]
           },
           {
              "featureType":"administrative",
              "elementType":"geometry.stroke",
              "stylers":[
                 {
                    "color":"#000000"
                 },
                 {
                    "lightness":17
                 },
                 {
                    "weight":1.2
                 }
              ]
           },
           {
              "featureType":"landscape",
              "elementType":"geometry",
              "stylers":[
                 {
                    "color":"#000000"
                 },
                 {
                    "lightness":20
                 }
              ]
           },
           {
              "featureType":"landscape",
              "elementType":"geometry.fill",
              "stylers":[
                 {
                    "color":"#4d6059"
                 }
              ]
           },
           {
              "featureType":"landscape",
              "elementType":"geometry.stroke",
              "stylers":[
                 {
                    "color":"#4d6059"
                 }
              ]
           },
           {
              "featureType":"landscape.natural",
              "elementType":"geometry.fill",
              "stylers":[
                 {
                    "color":"#4d6059"
                 }
              ]
           },
           {
              "featureType":"poi",
              "elementType":"geometry",
              "stylers":[
                 {
                    "lightness":21
                 }
              ]
           },
           {
              "featureType":"poi",
              "elementType":"geometry.fill",
              "stylers":[
                 {
                    "color":"#4d6059"
                 }
              ]
           },
           {
              "featureType":"poi",
              "elementType":"geometry.stroke",
              "stylers":[
                 {
                    "color":"#4d6059"
                 }
              ]
           },
           {
              "featureType":"road",
              "elementType":"geometry",
              "stylers":[
                 {
                    "visibility":"on"
                 },
                 {
                    "color":"#7f8d89"
                 }
              ]
           },
           {
              "featureType":"road",
              "elementType":"geometry.fill",
              "stylers":[
                 {
                    "color":"#7f8d89"
                 }
              ]
           },
           {
              "featureType":"road.highway",
              "elementType":"geometry.fill",
              "stylers":[
                 {
                    "color":"#7f8d89"
                 },
                 {
                    "lightness":17
                 }
              ]
           },
           {
              "featureType":"road.highway",
              "elementType":"geometry.stroke",
              "stylers":[
                 {
                    "color":"#7f8d89"
                 },
                 {
                    "lightness":29
                 },
                 {
                    "weight":0.2
                 }
              ]
           },
           {
              "featureType":"road.arterial",
              "elementType":"geometry",
              "stylers":[
                 {
                    "color":"#000000"
                 },
                 {
                    "lightness":18
                 }
              ]
           },
           {
              "featureType":"road.arterial",
              "elementType":"geometry.fill",
              "stylers":[
                 {
                    "color":"#7f8d89"
                 }
              ]
           },
           {
              "featureType":"road.arterial",
              "elementType":"geometry.stroke",
              "stylers":[
                 {
                    "color":"#7f8d89"
                 }
              ]
           },
           {
              "featureType":"road.local",
              "elementType":"geometry",
              "stylers":[
                 {
                    "color":"#000000"
                 },
                 {
                    "lightness":16
                 }
              ]
           },
           {
              "featureType":"road.local",
              "elementType":"geometry.fill",
              "stylers":[
                 {
                    "color":"#7f8d89"
                 }
              ]
           },
           {
              "featureType":"road.local",
              "elementType":"geometry.stroke",
              "stylers":[
                 {
                    "color":"#7f8d89"
                 }
              ]
           },
           {
              "featureType":"transit",
              "elementType":"geometry",
              "stylers":[
                 {
                    "color":"#000000"
                 },
                 {
                    "lightness":19
                 }
              ]
           },
           {
              "featureType":"water",
              "elementType":"all",
              "stylers":[
                 {
                    "color":"#2b3638"
                 },
                 {
                    "visibility":"on"
                 }
              ]
           },
           {
              "featureType":"water",
              "elementType":"geometry",
              "stylers":[
                 {
                    "color":"#2b3638"
                 },
                 {
                    "lightness":17
                 }
              ]
           },
           {
              "featureType":"water",
              "elementType":"geometry.fill",
              "stylers":[
                 {
                    "color":"#24282b"
                 }
              ]
           },
           {
              "featureType":"water",
              "elementType":"geometry.stroke",
              "stylers":[
                 {
                    "color":"#24282b"
                 }
              ]
           },
           {
              "featureType":"water",
              "elementType":"labels",
              "stylers":[
                 {
                    "visibility":"off"
                 }
              ]
           },
           {
              "featureType":"water",
              "elementType":"labels.text",
              "stylers":[
                 {
                    "visibility":"off"
                 }
              ]
           },
           {
              "featureType":"water",
              "elementType":"labels.text.fill",
              "stylers":[
                 {
                    "visibility":"off"
                 }
              ]
           },
           {
              "featureType":"water",
              "elementType":"labels.text.stroke",
              "stylers":[
                 {
                    "visibility":"off"
                 }
              ]
           },
           {
              "featureType":"water",
              "elementType":"labels.icon",
              "stylers":[
                 {
                    "visibility":"off"
                 }
              ]
           }
        ];

        let mapOptions = {
          center: new google.maps.LatLng(scope.vm.latitude, scope.vm.longitude),
          zoom: 4,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true,
          styles: mapStyle
        };

        map = new google.maps.Map(element[0].querySelector('.map'), mapOptions);
        controller.registerMap(map);
      }
    };
  }
}
