export class GeoLocationService {
  static NAME: string = 'GeoLocationService';

  static factory ($q, $window, $rootScope) {
    return new GeoLocationService($q, $window, $rootScope)
  }

  /* @ngInject */
  constructor ($q, $window, $rootScope) {
    this.$q = $q;
    this.$window = $window;
    this.$rootScope = $rootScope;
  }

  getCurrentPosition (options) {
    let deferred = this.$q.defer();

    if ('geolocation' in this.$window.navigator) {
      this.$window.navigator.geolocation.getCurrentPosition((position) => {
        this.$rootScope.$apply(() => {
          deferred.resolve(position);
        });
      }, (error) => {
        this.$rootScope.$apply(() => {
          deferred.reject(error);
        });
      });
    } else {
      deferred.reject('Not Supported');
    }

    return deferred.promise;
  }
}
