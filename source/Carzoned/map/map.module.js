import {MapDirective} from './map.directive';
import {MapDirectiveController} from './map.directive.controller';

import {GeoLocationService} from './geolocation.service';

import {LocationSearchDirective} from './locationsearch/locationsearch.directive';
import {LocationSearchDirectiveController} from './locationsearch/locationsearch.directive.controller';

import {GooglePlaceDirective} from './locationsearch/googleplace.directive';

import {MapService} from './map.service';

export default angular.module('carzoned.map', [])
  .directive(MapDirective.NAME, MapDirective.factory)
  .controller(MapDirectiveController.NAME, MapDirectiveController)
  .factory(GeoLocationService.NAME, GeoLocationService.factory)
  .directive(LocationSearchDirective.NAME, LocationSearchDirective.factory)
  .controller(LocationSearchDirectiveController.NAME, LocationSearchDirectiveController)
  .directive(GooglePlaceDirective.NAME, GooglePlaceDirective.factory)
  .service(MapService.NAME, MapService)
  .name;
