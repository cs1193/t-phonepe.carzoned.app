// Will contain map, pickup/destination and cards
import ngRoute from 'angular-route';

import {RideConfig} from './ride.config';
import {RideController} from './ride.controller';

import {RideCardDirective} from './ridecard/ridecard.directive';
import {RideCardDirectiveController} from './ridecard/ridecard.directive.controller';

import {RideService} from './ride.service';

import MapModule from '../map/map.module';
import ReviewModule from '../review/review.module';
import AssetFilteringModule from '../assetfiltering/assetfiltering.module';
import AuthenticationModule from '../authentication/authentication.module';

export default angular.module('carzoned.ride', [
    ngRoute,
    ReviewModule,
    MapModule,
    AssetFilteringModule,
    AuthenticationModule
  ])
  .constant('RIDE_ENDPOINT', RIDE_ENDPOINT)
  .constant('CONFIRM_RIDE_ENDPOINT', CONFIRM_RIDE_ENDPOINT)
  .directive(RideCardDirective.NAME, RideCardDirective.factory)
  .controller(RideCardDirectiveController.NAME, RideCardDirectiveController)
  .controller(RideController.NAME, RideController)
  .service(RideService.NAME, RideService)
  .config(RideConfig)
  .name;
