/* @flow */
import {AuthenticationRouteResolver} from '../authentication/authentication.routeresolver';

import {RideController} from './ride.controller';

import RideTemplate from './ride.html';

/* @ngInject */
export function RideConfig ($routeProvider, $httpProvider) {
  let rideRoute: object = {
    controller: RideController.NAME,
    controllerAs: 'vm',
    template: RideTemplate,
    resolve: {
      authentication: AuthenticationRouteResolver
    }
  };

  $routeProvider
    .when('/', rideRoute);
}
