import {RideService} from './ride.service';

export class RideController {
  static NAME: string = 'RideController';

  /* @ngInject */
  constructor ($log, $scope, RideService) {
    let vm = this;
    this.$log = $log;
    this.$scope = $scope;
    this.RideService = RideService;

    this.locationSearch = {
      originLocation: "",
      originCoords: [],
      destinationLocation: "",
      destinationCoords: []
    };

    this.filter = {
      selectedCarType: '',
      selectedSeat: '',
      selectedCarry: [],
      selectedRating: ''
    };

    this.isLoading = false;

    this.rides = [];

    $scope.$watchCollection(() => {
      return vm.locationSearch.originCoords
    }, (newValue, oldValue) => {
      if (newValue !== oldValue) {
        vm.updateRideList();
      }
    });

    $scope.$watchCollection(() => {
      return vm.locationSearch.destinationCoords
    }, (newValue, oldValue) => {
      if (newValue !== oldValue) {
        vm.updateRideList();
      }
    });

    $scope.$watchCollection(() => {
      return vm.filter
    }, (newValue, oldValue) => {
      if (newValue !== oldValue) {
        this.updateRideList();
      }
    });

  }

  onSelect (ride) {
    this.selected = ride;
  }

  updateRideList () {
    let vm = this;
    this.isLoading = true;
    this.RideService.getRidesByLocationAndFilter(this.locationSearch, this.filter).then((response) => {
      console.log(response);
      if (response && response.status === 'success') {
        this.errorMessage = '';
        this.rides = response.rides;
      } else if (response.status === 'error') {
        this.errorMessage = response.message;
      }
      this.isLoading = false;
    }, (error) => {
      this.errorMessage = 'No Rides Found.';
      this.$log.log(error);
      this.isLoading = false;
    });
  }

  askPickup (rideId) {
    this.RideService.setRideByUser(rideId).then((response) => {
      if (response && response && response.status === 'success') {
        this.errorMessage = '';
        this.confimId = rideId;
        this.confirmRide = response;
      } else if (response.status === 'error') {
        this.errorMessage = response.message;
      }
      this.isLoading = false;
    }, (error) => {
      this.errorMessage = 'Cannot confirm Ride.';
      this.$log.log(error);
      this.isLoading = false;
    });
  }
}
