import RideCardTemplate from './ridecard.directive.html';
import {RideCardDirectiveController} from './ridecard.directive.controller';

export class RideCardDirective {
  static NAME: string = 'rideCard';

  static factory () {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        rideId: '=',
        vehicleNo: '=',
        timeAway: '=',
        seatLeft: '=',
        ownerName: '=',
        make: '=',
        model: '=',
        color: '=',
        onSelect: '&',
        askPickup: '&',
        confirmRide: '='
      },
      template: RideCardTemplate,
      controller: RideCardDirectiveController,
      controllerAs: 'vm',
      bindToController: true
    };
  }
}
