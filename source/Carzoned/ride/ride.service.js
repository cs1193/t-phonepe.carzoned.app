export class RideService {
  static NAME: string = 'RideService';

  /* @ngInject */
  constructor ($log, $q, $http, RIDE_ENDPOINT, CONFIRM_RIDE_ENDPOINT) {
    this.$log = $log;
    this.$q = $q;
    this.$http = $http;
    this.RIDE_ENDPOINT = RIDE_ENDPOINT;
    this.CONFIRM_RIDE_ENDPOINT = CONFIRM_RIDE_ENDPOINT;
  }

  getRidesByLocationAndFilter (location, filter) {
    let deferred = this.$q.defer();

    let isFilterPresent = {
      cartype: (filter.selectedCarType) ? `/${filter.selectedCarType}` : ``,
      seat: (filter.selectedSeat) ? `/${filter.selectedSeat}` : ``,
      rating: (filter.selectedRating) ? `/${filter.selectedRating}` : ``,
      carry: (filter.selectedCarry.length > 0) ?
                (filter.selectedCarry == 2) ? '/' + filter.selectedCarry[0] + '/' + filter.selectedCarry[1] :
                (filter.selectedCarry == 1) ? '/' + filter.selectedCarry[0] : ''
                : ''
    };

    let url = `${this.RIDE_ENDPOINT}/${location.originCoords[0]}/${location.originCoords[1]}/${location.destinationCoords[0]}/${location.destinationCoords[1]}${isFilterPresent.cartype}${isFilterPresent.seat}${isFilterPresent.rating}${isFilterPresent.carry}`;

    this.$http.get(url).then((response) => {
      if (response && response.data && response['status'] === 200 || response['status'] === 201) {
        deferred.resolve(response.data);
      }
    }, (error) => {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  setRideByUser (rideId) {
    let deferred = this.$q.defer();

    this.$http.post(this.CONFIRM_RIDE_ENDPOINT, {
      rideid: rideId
    }).then((response) => {
      if (response && response.data && response['status'] === 200 || response['status'] === 201) {
        deferred.resolve(response.data);
      }
    }, (error) => {
      deferred.reject(error);
    });

    return deferred.promise;
  }
}
