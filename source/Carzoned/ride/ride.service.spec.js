import RideModule from './ride.module';
import {RideService} from './ride.service';

describe('Service: Ride', () => {

  let httpBackend;
  let $rootScope;
  let $q;
  let RideService;

  const fakeRideEndpoint = "http://fakerideendpoint/23/23/32/32/";
  const fakeConfirmRideEndpoint = "http://fakeconfirmrideendpoint/";

  const fakeResolveLocationSearch = {
    originLocation: 'Start Place',
    originCoords: {
      lat: () => {
        return '2.221';
      },
      lng: () => {
        return '3.444';
      }
    },
    destinationLocation: 'End Place',
    destinationCoords: {
      lat: () => {
        return '5.66';
      },
      lng: () => {
        return '7.88';
      }
    }
  };

  const fakeResolveFilter = {
    selectedSeat: 2,
    selectedCarry: ['luggage'],
    selectedRating: 4,
    selectedCarType: 'sedan'
  };

  const fakeResolveRide = {
    'data': {
      'status': 'success',
      'rides': [{
        'id': '1',
        'vehicleNo': 'TN 10 AQ 6966',
        'timeAway': '5',
        'seatLeft': '2',
        'ownerName': 'Chandresh',
        'make': 'Toyota',
        'model': 'Camry',
        'color': 'Black',
        'carType': 'sedan',
        'rating': '2',
        'luggageAllowed': false,
        'petAllowed': true
      }, {
        'id': '2',
        'vehicleNo': 'TN 10 AQ 6966',
        'timeAway': '5',
        'seatLeft': '2',
        'ownerName': 'Chandresh',
        'make': 'Toyota',
        'model': 'Camry',
        'color': 'Black',
        'carType': 'coupe',
        'rating': '3',
        'luggageAllowed': false,
        'petAllowed': false
      }]
    }
  };

  let fakeNoRidesResolve = {
    'data': {
      'status': 'error',
      'message': 'No Rides Found'
    }
  };

  let confirmRideAccepted = {
    'data': {
      'status': 'success',
      'message': 'accepted',
      'data': {
        'name': 'someone',
        'phone': '+91 9888888888'
      }
    }
  };

  let confirmRideDeclined = {
    'data': {
      'status': 'success',
      'message': 'declined'
    }
  };

  let confirmRideError = {
    'data': {
      'status': 'error',
      'message': 'error'
    }
  };

  beforeEach(angular.mock.module(RideModule));

  beforeEach(angular.mock.module(($provide) => {
    $provide.constant('RIDE_ENDPOINT', fakeRideEndpoint);
    $provide.constant('CONFIRM_RIDE_ENDPOINT', fakeConfirmRideEndpoint);
  }));

  beforeEach(inject((
    _$q_,
    _$rootScope_,
    _$httpBackend_,
    _RideService_
  ) => {
    $q = _$q_;
    $rootScope = _$rootScope_;
    httpBackend = _$httpBackend_;
    RideService = _RideService_;
  }));

  it('should retrieve service data success on correct parameter', () => {
    httpBackend.expectGET(fakeRideEndpoint).respond(200, fakeResolveRide);
    var rides = [];
    RideService.getRidesByLocationAndFilter(fakeResolveLocationSearch, fakeResolveFilter).then((response) => {
      if (response.data.status === 'success') {
        rides = response.data.rides;
        expect(rides.length).toBe(2);
      }
    });
  });

  it('should retrieve service data no rides found on unavailable correct parameter', () => {
    httpBackend.expectGET(fakeRideEndpoint).respond(200, fakeNoRidesResolve);
    RideService.getRidesByLocationAndFilter(fakeResolveLocationSearch, fakeResolveFilter).then((response) => {
      if (response.data.status === 'error') {
        var message = response.message;
        expect(message).toEqual('No Rides Found');
      }
    });
  });

  it('should be accepted on confirm ride', () => {
    let message = '';
    httpBackend.expectPOST(fakeConfirmRideEndpoint).respond(200, confirmRideAccepted);
    RideService.setRideByUser('1').then((response) => {
      if (response.data.status === 'success') {
        message = response.data.message;
      }
    });
    httpBackend.flush();
    expect(message).toEqual('accepted');
  });

  it('should be rejected on confirm ride', () => {
    let message = '';
    httpBackend.expectPOST(fakeConfirmRideEndpoint).respond(confirmRideDeclined);
    RideService.setRideByUser('1').then((response) => {
      if (response.data.status === 'success') {
        message = response.data.message;
      }
    });
    httpBackend.flush();
    expect(message).toEqual('declined');
  });

  it('should be an error on confirm ride', () => {
    let message = '';
    httpBackend.expectPOST(fakeConfirmRideEndpoint).respond(200, confirmRideError);
    RideService.setRideByUser('1').then((response) => {
      if (response.data.status === 'error') {
        message = response.data.message;
      }
    });
    httpBackend.flush();
    expect(message).toEqual('error');
  });

});
