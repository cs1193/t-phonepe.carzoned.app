/* @flow */
import {AuthenticationController} from './authentication.controller';
import {RegisterController} from './register.controller';

import {AuthenticationRouteResolver} from './authentication.routeresolver';
import {AuthenticationHttpInterceptor} from './authentication.httpinterceptor';

import LoginTemplate from './login.html';
import LogoutTemplate from './logout.html';
import RegisterTemplate from './register.html';

/* @ngInject */
export function AuthenticationConfig ($routeProvider, $httpProvider, $localStorageProvider) {
  $localStorageProvider.setKeyPrefix('carzoned-auth-');

  let loginRoute: object = {
    controller: AuthenticationController.NAME,
    controllerAs: 'vm',
    template: LoginTemplate
  };

  let logoutRoute: object = {
    template: LogoutTemplate,
    controller: AuthenticationController.NAME,
    controllerAs: 'vm',
    resolve: {
      authenticate: AuthenticationRouteResolver
    }
  };

  let registerRoute: object = {
    controller: RegisterController.NAME,
    controllerAs: 'vm',
    template: RegisterTemplate
  };

  $routeProvider
    .when('/login', loginRoute)
    .when('/logout', logoutRoute)
    .when('/register', registerRoute);

  $httpProvider.interceptors.push(AuthenticationHttpInterceptor.NAME);

}
