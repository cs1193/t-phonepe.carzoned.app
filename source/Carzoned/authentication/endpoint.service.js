import {AuthenticationService} from './authentication.service';

export class EndpointService {
  static NAME: string = "EndpointService";

  /* @ngInject */
  constructor ($q, $http, AuthenticationService, LOGIN_URL, LOGOUT_URL, REGISTER_URL) {
    this.$q = $q;
    this.$http = $http;
    this.AuthenticationService = AuthenticationService;
    this.LOGIN_URL = LOGIN_URL;
    this.LOGOUT_URL = LOGOUT_URL;
    this.REGISTER_URL = REGISTER_URL;
  }

  login (email: string, password: string) : Promise {
    let deferred = this.$q.defer();

    this.$http.post(this.LOGIN_URL, {
      email: email,
      password: password
    }).then((response) => {
      if (response && response.data && response['status'] === 200 || response['status'] === 201) {
        deferred.resolve(response.data);
      }
    }, (error) => {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  logout () : Promise {
    let deferred = this.$q.defer();

    this.$http.post(this.LOGOUT_URL).then((response) => {
      if (response && response.data && response['status'] === 200 || response['status'] === 201) {
        deferred.resolve();
      }
    }, (error) => {
      deferred.reject();
    });

    return deferred.promise;
  }

  register (name: string, email: string, password: string, phone: string) : Promise {
    let deferred = this.$q.defer();

    this.$http.post(this.REGISTER_URL, {
      name: name,
      email: email,
      phone: phone,
      password: password
    }).then((response) => {
      if (response && response.data && response['status'] === 200 || response['status'] === 201) {
        deferred.resolve(response.data);
      }
    }, (error) => {
      deferred.reject();
    });

    return deferred.promise;
  }
}
