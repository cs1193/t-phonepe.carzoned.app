import {EndpointService} from './endpoint.service';
import {AuthenticationService} from './authentication.service';

export class AuthenticationController {
  static NAME: string = "AuthenticationController";

  /* @ngInject */
  constructor ($log, $location, $timeout, EndpointService, AuthenticationService) {
    this.$log = $log;
    this.$location = $location;
    this.$timeout = $timeout;
    this.EndpointService = EndpointService;
    this.AuthenticationService = AuthenticationService;
    this.isLoading = false;
  }

  login () {
    this.isLoading = true;
    this.EndpointService.login(this.loginForm.email, this.loginForm.password).then((response) => {
      if (response && response.status === 'success') {
        this.AuthenticationService.setAccessToken(response.token);
        this.$location.url('/');
      } else {
        this.errorMessage = response.message;
        this.$log.log(response);
      }
      this.isLoading = false;
    }, (error) => {
      this.errorMessage = 'Login Error';
      this.$log.log(error);
      this.isLoading = false;
    });
  }

  logout () {
    this.isLoading = true;
    if (this.AuthenticationService.isAuthenticated()) {
      this.EndpointService.logout().then((response) => {
        if (response) {
          this.AuthenticationService.removeAccessToken();
          this.$location.url('/login');
        }
        this.isLoading = false;
      }, (error) => {
        this.$log.log(error);
      });
    } else {
      this.AuthenticationService.redirectToLogin();
    }
  }
}
