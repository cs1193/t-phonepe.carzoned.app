import ngRoute from 'angular-route';
import ngMessages from 'angular-messages';
import 'ngstorage';

import {AuthenticationController} from './authentication.controller';
import {RegisterController} from './register.controller';

import {AuthenticationService} from './authentication.service';
import {EndpointService} from './endpoint.service';

import {AuthenticationHttpInterceptor} from './authentication.httpinterceptor';

import {AuthenticationConfig} from './authentication.config';

import UtilitiesModule from '../utilities/utilities.module';

export default angular.module('carzoned.authentication', [
    ngRoute,
    ngMessages,
    'ngStorage',
    UtilitiesModule
  ])
  .constant('LOGIN_URL', LOGIN_URL)
  .constant('LOGOUT_URL', LOGOUT_URL)
  .constant('REGISTER_URL', REGISTER_URL)
  .factory(AuthenticationHttpInterceptor.NAME, AuthenticationHttpInterceptor.factory)
  .service(AuthenticationService.NAME, AuthenticationService)
  .service(EndpointService.NAME, EndpointService)
  .controller(AuthenticationController.NAME, AuthenticationController)
  .controller(RegisterController.NAME, RegisterController)
  .config(AuthenticationConfig)
  .name;
