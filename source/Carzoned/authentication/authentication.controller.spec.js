import AuthenticationModule from './authentication.module';
import {AuthenticationController} from './authentication.controller';
import {AuthenticationService} from './authentication.service';
import {EndpointService} from './endpoint.service';

describe('Controller: Authentication', () => {
  let $rootScope;
  let scope;
  let $location;
  let controller;
  let $controller;
  let $httpBackend;
  let $timeout;

  let EndpointService;
  let AuthenticationService;

  let fakeLoginUrl = '/fake-login-url';
  let fakeLogoutUrl = '/fake-logout-url';

  beforeEach(angular.mock.module(AuthenticationController));

  beforeEach(angular.mock.module(($provide) => {
    $provide.constant('LOGIN_URL', fakeLoginUrl);
    $provide.constant('LOGOUT_URL', fakeLogoutUrl);
  }));

  beforeEach(inject((
    _$log_,
    _$location_,
    _$timeout_,
    _$rootScope_,
    _$controller_,
    _$httpBackend_,
    _EndpointService_,
    _AuthenticationService_
  ) => {
    $location = _$location_;
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    $timeout = _$timeout_;
    $log = _$log_;

    EndpointService = _EndpointService_;
    AuthenticationService = _AuthenticationService_;

    scope = $rootScope.$new();

    controller = $controller(AuthenticationController.NAME, {
      $log: $log,
      $location: $location,
      $timeout: $timeout,
      EndpointService: EndpointService,
      AuthenticationService: AuthenticationService
    });

    spyOn(EndpointService, 'login').and.return(_$q_.when({'messge': 'hello'}));
    spyOn(EndpointService, 'logout').and.return(_$q_.when({'messge': 'hello'}));

    $rootScope.$digest();
  }));

});
