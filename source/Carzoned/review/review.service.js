export class ReviewService {
  static NAME: string = "ReviewService";

  /* @ngInject */
  constructor ($q, $http, REVIEW_URL) {
    this.$q = $q;
    this.$http = $http;
    this.REVIEW_URL = REVIEW_URL;
  }
}
