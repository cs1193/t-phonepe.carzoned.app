import StarRatingTemplate from './starrating.directive.html';
import {StarRatingDirectiveController} from './starrating.directive.controller';

export class StarRatingDirective {
  static NAME: string = 'starRating';

  static factory () {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        currentRating: '@',
        maxRating: '@',
        withText: '@',
        isClickable: '@',
        isEditable: '@'
      },
      controller: StarRatingDirectiveController.NAME,
      controllerAs: 'vm',
      bindToController: true,
      template: StarRatingTemplate,
      link: function (scope, element, attributes) {
        scope.vm.maxRating = (scope.vm.maxRating) ? scope.vm.maxRating : 5;

        function updateRating () {
          scope.vm.stars = [];

          for (let i = 0; i < scope.vm.maxRating; i++) {
            scope.vm.stars.push({
              active: i < scope.vm.currentRating
            });
          }
        }

        scope.$watch(() => scope.vm.currentRating, (oldValue, newValue) => {
          if (newValue || newValue === 0)
            updateRating();
        });
      }
    };
  }
}
