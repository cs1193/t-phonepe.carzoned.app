import ngAnimate from 'angular-animate';

import {ReviewService} from './review.service';

import {StarRatingDirective} from './starrating/starrating.directive';
import {StarRatingDirectiveController} from './starrating/starrating.directive.controller';

export default angular.module('carzoned.review', [
    ngAnimate
  ])
  .directive(StarRatingDirective.NAME, StarRatingDirective.factory)
  .controller(StarRatingDirectiveController.NAME, StarRatingDirectiveController)
  .service(ReviewService.NAME, ReviewService)
  .name;
