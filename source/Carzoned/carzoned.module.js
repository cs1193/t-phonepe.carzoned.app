import ngRoute from 'angular-route';
import ngAnimate from 'angular-animate';

import {CarzonedConfig} from './carzoned.config';

import {CarzonedDirective} from './carzoned.directive';
import {CarzonedDirectiveController} from './carzoned.directive.controller';

import AuthenticationModule from './authentication/authentication.module';
import RideModule from './ride/ride.module';
import UtilitiesModule from './utilities/utilities.module';

export default angular.module('carzoned', [
    ngRoute,
    ngAnimate,
    AuthenticationModule,
    RideModule,
    UtilitiesModule
  ])
  .directive(CarzonedDirective.NAME, CarzonedDirective.factory)
  .controller(CarzonedDirectiveController.NAME, CarzonedDirectiveController)
  .config(CarzonedConfig)
  .name;
