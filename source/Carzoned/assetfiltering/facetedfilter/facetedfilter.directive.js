import FacetedFilterDirectiveTemplate from './facetedfilter.directive.html';
import {FacetedFilterDirectiveController} from './facetedfilter.directive.controller';

export class FacetedFilterDirective {
  static NAME: string = 'facetedFilter';

  static factory () {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        filterData: '=',
        selectedSeat: '=',
        selectedCarry: '=',
        selectedCarType: '=',
        selectedRating: '='
      },
      template: FacetedFilterDirectiveTemplate,
      controller: FacetedFilterDirectiveController.NAME,
      controllerAs: 'vm',
      bindToController: true
    };
  }
}
