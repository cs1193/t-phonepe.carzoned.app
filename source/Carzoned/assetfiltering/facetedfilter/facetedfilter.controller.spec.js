import AssetFilteringModule from '../assetfiltering.module';
import {FacetedFilterDirectiveController} from './facetedfilter.directive';

describe('Controller: Faceted Filter Directive Controller', () => {
  let $rootScope;
  let scope;
  let controller;
  let $controller;

  beforeEach(angular.mock.module(AssetFilteringModule));

  beforeEach(inject((
    _$rootScope_,
    _$controller_
  ) => {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    scope = $rootScope.$new();

    controller = $controller('FacetedFilterDirectiveController', {
      $scope: scope
    });
  }));

  it('should set selectedSeat as two if selectFilter with seat and two as parameters', () => {
    controller.selectFilter('seat', 2);
    expect(controller.selectedSeat).toBe(2);
  });
});
