export class FacetedFilterDirectiveController {
  static NAME: string = 'FacetedFilterDirectiveController';

  /* @ngInject */
  constructor ($scope) {
    let vm = this;

    $scope.$watch(() => vm.filterData, (newValue, oldValue) => {
      if (newValue.length > 0) {
        let seats = newValue.map((item) => {
          return item.seatLeft;
        });

        vm.seatsAvailable = seats.filter((value, index) => {
          return seats.indexOf(value) == index;
        });

        vm.isLuggageAllowed = newValue.filter((item) => {
          return item.luggageAllowed;
        }).length > 0;

        vm.isPetAllowed = newValue.filter((item) => {
          return item.petAllowed;
        }).length > 0;

        let cartype = newValue.map((item) => {
          return item.carType;
        });

        vm.carTypes = cartype.filter((value, index) => {
          return cartype.indexOf(value) == index;
        });

        let rating = newValue.map((item) => {
          return item.rating;
        });

        vm.ratings =  rating.filter((value, index) => {
          return rating.indexOf(value) == index;
        }).sort((a, b) => {
          return b - a;
        });
      }
    }, true);
  }

  selectFilter (type, value) {
    let vm = this;
    switch (type) {
      case 'seat':
        vm.selectedSeat = value;
        break;
      case 'carry':
        vm.selectedCarry = [];
        if (value === 'luggage') {
          vm.selectedCarry[0] = 'luggage'
        }

        if (value === 'pet') {
          vm.selectedCarry[1] = 'pet';
        }
        break;

      case 'cartype':
        vm.selectedCarType = value;
        break;

      case 'rating':
        vm.selectedRating = value;
        break;
    }
  }
}
