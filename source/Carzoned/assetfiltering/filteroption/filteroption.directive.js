import FilterOptionDirectiveTemplate from './filteroption.directive.html';

export class FilterOptionDirective {
  static NAME: string = 'filterOption';

  static factory () {
    return {
      restrict: 'E',
      scope: {
        title: '@',
        selectMultiple: '@',
        showControls: '@'
      },
      transclude: true,
      template: FilterOptionDirectiveTemplate
    };
  }
}
