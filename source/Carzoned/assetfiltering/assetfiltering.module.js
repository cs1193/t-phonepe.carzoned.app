import {FacetedFilterDirective} from './facetedfilter/facetedfilter.directive';
import {FacetedFilterDirectiveController} from './facetedfilter/facetedfilter.directive.controller';

import {FilterOptionDirective} from './filteroption/filteroption.directive';

import ReviewModule from '../review/review.module';
import UtilitiesModule from '../utilities/utilities.module';

export default angular.module('carzoned.assetfiltering', [
    ReviewModule,
    UtilitiesModule
  ])
  .directive(FacetedFilterDirective.NAME, FacetedFilterDirective.factory)
  .controller(FacetedFilterDirectiveController.NAME, FacetedFilterDirectiveController)
  .directive(FilterOptionDirective.NAME, FilterOptionDirective.factory)
  .name;
