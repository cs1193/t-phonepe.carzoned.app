import CarzonedTemplate from './carzoned.directive.html';
import {CarzonedDirectiveController} from './carzoned.directive.controller';

export class CarzonedDirective {
  static NAME: string = 'carzoned';

  static factory () {
    return {
      restrict: 'E',
      replace: true,
      template: CarzonedTemplate,
      controller: CarzonedDirectiveController,
      controllerAs: 'vm',
      bindToController: true
    };
  }
}
